import { IDB } from "../../db/interfaces";
import { ITodo, ITodoWithoutCompleted } from "./interfaces";
import TodoService from "./todo.service";
import * as moduleToTest from "./todo.service";
import { todoService } from "./todo.container";

let fakeService: TodoService;
let fakeDB: IDB<ITodo>;
let todoTest: ITodoWithoutCompleted;
const todoDate = new Date(2001, 3, 21);
const todoUpdateDate = new Date(2020, 5, 13);
describe("Todo Service UT's", () => {
  beforeEach(() => {
    fakeDB = {
      get: jest.fn(),
      getAll: jest.fn(),
      add: jest.fn(),
      edit: jest.fn(),
      remove: jest.fn(),
    };
    fakeService = new TodoService(fakeDB);
  });
  todoTest = {
    listId: "test",
    task: "buy tomato",
    finishDate: todoDate,
  };
  it("Adds todo", () => {
    moduleToTest.__Rewire__("checkIfListExist", () => {
      return true;
    });

    fakeService.add(todoTest);
    expect(checkIfListExist).toBeCalled();
    expect(fakeService.add).toBeCalled();
  });

  it("Edits todo", () => {
    moduleToTest.__Rewire__("checkIfListExist", () => {
      return true;
    });

    fakeService.edit("fake-id", todoTest);
    expect(checkIfListExist).toBeCalled();
    expect(fakeService.safeGetById).toBeCalledWith("fake-id");
    expect(fakeService.edit).toBeCalledWith("fake-id", todoTest);
  });

  it("Edits todos date", () => {
    fakeService.editDate("fake-id", todoUpdateDate);
    expect(fakeService.safeGetById).toBeCalledWith("fake-id");
    todoTest.finishDate = todoUpdateDate;
    expect(fakeService.edit).toBeCalledWith("fake-id", todoTest);
  });

  it("Toggles isComplete", () => {
    fakeService.toggle("fake-id");
    expect(fakeService.safeGetById).toBeCalledWith('fake-id');
    expect(fakeService.edit).toBeCalledWith('fake-id',)
  });

  it("Deletes task",()=>{
    fakeService.remove('fake-id')
    expect(fakeDB.remove).toBeCalledWith('fake-id')
  })
  
  it("Gets all tasks",()=>{
    fakeService.getAll();
    expect(fakeDB.getAll).toBeCalled();
  })

  it("Gets task by id",()=>{
    fakeService.getByID('fake-id');
    expect(fakeDB.get).toBeCalledWith('fake-id')
  })

});
